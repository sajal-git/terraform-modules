resource "aws_key_pair" "nat" {
  key_name = "${var.keyname}"
  public_key = "${file("${var.public_key_path}")}"
}
