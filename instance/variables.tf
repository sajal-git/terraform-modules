variable "ami_id" {}
variable "ins_type" {}
variable "subnet_id" {}
variable "key_name" {}
variable "vpc_security_groups" { type = "list" }
variable "pub_ip" {}
variable "pvt_ip" {}
variable "vol_size" {}
variable "name" {}
