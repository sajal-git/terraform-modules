resource "aws_instance" "ins" {
  ami = "${var.ami_id}"
  instance_type = "${var.ins_type}"
  subnet_id = "${var.subnet_id}"
  key_name = "${var.key_name}"
  vpc_security_group_ids = ["${var.vpc_security_groups}"]
  associate_public_ip_address = "${var.pub_ip}"
  private_ip = "${var.pvt_ip}"
#  disable_api_termination = true
  root_block_device {
    volume_type = "gp2"
    volume_size = "${var.vol_size}"
    delete_on_termination = true
  }

  tags {
    Name = "${var.name}"
  }
}
