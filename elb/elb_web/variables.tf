variable "elb_name" {}
variable "elb_sg" {
  type = "list"
}
variable "subnet_ids" {
  type = "list"
}
variable "instance_ids" {
  type = "list"
}
