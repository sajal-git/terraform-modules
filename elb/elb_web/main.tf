resource "aws_elb" "elb" {
  name = "${var.elb_name}"

  listener {
    instance_port = 22
    instance_protocol = "tcp"
    lb_port = 22
    lb_protocol = "tcp"
  }

  listener {
    instance_port = 80 
    instance_protocol = "http"
    lb_port = 80
    lb_protocol = "http"
  }

/*  listener {
    instance_port = 443 
    instance_protocol = "https"
    lb_port = 443
    lb_protocol = "https"
  }*/

  health_check {
    healthy_threshold = 2
    unhealthy_threshold = 2
    timeout = 3
    target = "TCP:80"
    interval = 30
  }

  subnets = ["${var.subnet_ids}"]
  instances = ["${var.instance_ids}"]
  security_groups = ["${var.elb_sg}"]
  cross_zone_load_balancing = true
  idle_timeout = 400
  connection_draining = true
  connection_draining_timeout = 400

  tags {
    Name = "${var.elb_name}"
  }

}
