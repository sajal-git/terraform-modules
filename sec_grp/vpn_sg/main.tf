resource "aws_security_group" "vpn_sg" {
  name = "${var.name}"
  description = "Allow inbound traffic"
  vpc_id      = "${var.vpcid}"

  ingress {
      from_port = 1194
      to_port = 1194
      protocol = "udp"
      cidr_blocks = ["${var.source_cidr}"]
  }
  ingress {
      from_port = 943
      to_port = 943
      protocol = "tcp"
      cidr_blocks = ["${var.source_cidr}"]
  }

  egress {
      from_port = 0
      to_port = 0
      protocol = "-1"
      cidr_blocks = ["0.0.0.0/0"]
  } 

  tags {
    Name = "${var.tagname}"
  }
}
