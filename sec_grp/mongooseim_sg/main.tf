resource "aws_security_group" "mongoose_sg" {
  name = "${var.name}"
  description = "Allow inbound traffic"
  vpc_id      = "${var.vpcid}"

  ingress {
      from_port = 50000
      to_port = 50010
      protocol = "tcp"
      cidr_blocks = ["${var.source_cidr}"]
  }
  ingress {
      from_port = 5222
      to_port = 5223
      protocol = "tcp"
      cidr_blocks = ["${var.source_cidr}"]
  }
  ingress {
      from_port = 5280
      to_port = 5280
      protocol = "tcp"
      cidr_blocks = ["${var.source_cidr}"]
  }
  ingress {
      from_port = 5288
      to_port = 5288
      protocol = "tcp"
      cidr_blocks = ["${var.source_cidr}"]
  }
  ingress {
      from_port = 9042
      to_port = 9042
      protocol = "tcp"
      cidr_blocks = ["${var.source_cidr}"]
  }
  ingress {
      from_port = 9160
      to_port = 9160
      protocol = "tcp"
      cidr_blocks = ["${var.source_cidr}"]
  }

  egress {
      from_port = 0
      to_port = 0
      protocol = "-1"
      cidr_blocks = ["0.0.0.0/0"]
  } 

  tags {
    Name = "${var.tagname}"
  }
}
