resource "aws_security_group" "tungsten_sg" {
  name = "${var.name}"
  description = "Allow inbound traffic"
  vpc_id      = "${var.vpcid}"

  ingress {
      from_port = 2112
      to_port = 2114
      protocol = "tcp"
      cidr_blocks = ["${var.source_cidr}"]
  }
  ingress {
      from_port = 10000
      to_port = 10003
      protocol = "tcp"
      cidr_blocks = ["${var.source_cidr}"]
  }
  ingress {
      from_port = 13306
      to_port = 13306
      protocol = "tcp"
      cidr_blocks = ["${var.source_cidr}"]
  }
  ingress {
      from_port = 7
      to_port = 7
      protocol = "tcp"
      cidr_blocks = ["${var.source_cidr}"]
  }

  egress {
      from_port = 0
      to_port = 0
      protocol = "-1"
      cidr_blocks = ["0.0.0.0/0"]
  } 

  tags {
    Name = "${var.tagname}"
  }
}
