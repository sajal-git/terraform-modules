resource "aws_security_group" "windows_sg" {
  name = "${var.name}"
  description = "Allow inbound traffic"
  vpc_id      = "${var.vpcid}"

  ingress {
      from_port = 1433
      to_port = 1433
      protocol = "tcp"
      cidr_blocks = ["${var.source_cidr}"]
  }
  ingress {
      from_port = 3389
      to_port = 3389
      protocol = "tcp"
      cidr_blocks = ["${var.source_cidr}"]
  }

  egress {
      from_port = 0
      to_port = 0
      protocol = "-1"
      cidr_blocks = ["0.0.0.0/0"]
  } 

  tags {
    Name = "${var.tagname}"
  }
}
